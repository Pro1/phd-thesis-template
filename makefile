#
# simply compiling our paper and extracting useful information from the log

MAINSRC := Thesis.tex

SOURCES := $(shell sed -E -e '/input/ s%^.input\{(.*)\}.*%\1.tex% p; d;' $(MAINSRC))
BIBSOURCES := $(wildcard *.bib)

# keep track of error detected early, when we do not (yet) want to stopp.
REFSTATEFILE := .referror

all: $(MAINSRC:.tex=.pdf)


$(MAINSRC:.tex=.pdf): $(MAINSRC) $(MAINSRC:.tex=.bbl) $(SOURCES) $(BIBSOURCES)
	pdflatex $<
	# With makeglossary:
	makeglossaries $(MAINSRC:.tex=)
	@if grep -E -q -e 'Citation.*undefined' $(MAINSRC:.tex=.log) ; then \
	   bibtex $(MAINSRC:.tex=.aux) ; fi
	   pdflatex $<
	@if grep -E -q -e Rerun $(MAINSRC:.tex=.log) ; then \
	   pdflatex $< ; fi
	@if [ -e $(REFSTATEFILE) ] ; then rm $(REFSTATEFILE) ; fi
	@if cat $(MAINSRC:.tex=.log) | tr -d '\n' | grep -q -Pzo "Reference.*?undefined on input line \d+\." ; then \
	   echo "########### Undefined reference(s): ##############"; cat $(MAINSRC:.tex=.log) | tr -d '\n' | grep -Pzo "Reference.*?undefined on input line \d+\." ; touch $(REFSTATEFILE) ; fi
	@if cat $(MAINSRC:.tex=.log) | tr -d '\n' | grep -q -Pzo "Citation.*?undefined on input line \d+\." ; then \
	   echo "########### Undefined citation(s): ##############"; cat $(MAINSRC:.tex=.log) | tr -d '\n' | grep -Pzo "Citation.*?undefined on input line \d+\." ; touch $(REFSTATEFILE) ; fi
	@test ! -e $(REFSTATEFILE)

$(MAINSRC:.tex=.bbl): $(MAINSRC:.tex=.aux) $(MAINSRC) $(SOURCES) $(BIBSOURCES)
	biber $(MAINSRC:.tex=)

$(MAINSRC:.tex=.aux): $(MAINSRC) $(SOURCES) $(BIBSOURCES)
	pdflatex $<


clean:
	rm -vf $(SOURCES:.tex=.aux) $(MAINSRC:.tex=.log) $(MAINSRC:.tex=.pdf) $(MAINSRC:.tex=.chk)


find_sources:
	@for i in $(SOURCES) ; do echo $$i ; done


find_labels:
	grep -E -e '\\label\{' $(SOURCES)


find_labels2:
	@for i in $(SOURCES) ; do echo "$$i:" ; \
	   sed -E -e '/\\label\{/ s%.*label\{([^{}]+)\}.*%	\1% p; d;' <$$i; done


find_refs:
		grep -E -e '\\(page)?ref\{' $(SOURCES)

find_refs2:
	@for i in $(SOURCES) ; do echo "$$i:" ; \
	   sed -E -e '/\\(page)?ref\{/ s%.*(page)?ref\{([^{}]+)\}.*%	\2% p; d;' <$$i; done


help:
	@echo "Supported 'make' targets:"
	@echo " (none)      : calling make without target will build the paper PDF."
	@echo "find_labels  : produce a list of all defined labels, suitable for"
	@echo "               post-processing with other tools."
	@echo "find_labels2 : produce a human-readable list of all defined labels with their"
	@echo "               respective files."
	@echo "find_refs    : produce a list of all used references, suitable for"
	@echo "               post-processing with other tools."
	@echo "find_refs2   : produce a human-readable list of all used references with their"
	@echo "               respective files."
	@echo "clean        : remove all LaTeX auto-generated files."
	@echo "check        : Check for syntax errors and warnings."
	@echo "help         : This list."

check: $(MAINSRC:.tex=.chk)

$(MAINSRC:.tex=.chk):  $(MAINSRC) $(SOURCES)
	@chktex -l .chktexrc $(MAINSRC) > $(MAINSRC:.tex=.chk); if test -s $(MAINSRC:.tex=.chk) ; then \
		echo "########### Syntax errors: ##############"; cat $(MAINSRC:.tex=.chk) ; rm $(MAINSRC:.tex=.chk) ; false ; fi


.PHONY: all clean find_sources find_labels find_labels2 find_refs find_refs2 help check

makefile.dep: $(MAINSRC) makefile
	@echo "$(MAINSRC:.tex=.pdf): \\" >$@
	@for i in $(SOURCES) ; do echo "	$$i \\" ; done >>$@
	@echo >>$@

-include makefile.dep
