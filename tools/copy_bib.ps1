$MainDir = "$PSScriptRoot/.."

$sourceBib = "../PHD.bib"

$sourceTimeFile = $MainDir + "\bibtex\.lastCopyTime.txt"

$lastChangeTime = (Get-ItemProperty -Path $sourceBib -Name LastWriteTime).LastWriteTime

if (Test-Path  ($sourceTimeFile))
{
    $lastCopyTime = [datetime]::Parse((Get-Content -Path $sourceTimeFile -TotalCount 1))
    if ($lastCopyTime -eq $lastChangeTime)
    {
        Write-Host "File did not change"
        exit
    }
}
Set-Content -Path $sourceTimeFile -Value ($lastChangeTime.toString("o"))

Copy-Item $sourceBib -Destination "$MainDir\bibtex\PHD_unsorted.bib" -force

# Replace mendeley chunk at beginning of file, then replace invalid month names with numbers
(Get-Content "$MainDir\bibtex\PHD_unsorted.bib").Where({ $_ -like "*@*" },'SkipUntil') |
        Foreach-Object {$_ -replace "month\s*=\s*{(j|J)an(uary)?}", "month = 1"} |
        Foreach-Object {$_ -replace "month\s*=\s*{(f|F)eb(ruary)?}", "month = 2"} |
        Foreach-Object {$_ -replace "month\s*=\s*{(m|M)ar(ch)?}", "month = 3"} |
        Foreach-Object {$_ -replace "month\s*=\s*{(a|A)pr(il)?}", "month = 4"} |
        Foreach-Object {$_ -replace "month\s*=\s*{(m|M)ay}", "month = 5"} |
        Foreach-Object {$_ -replace "month\s*=\s*{(j|J)un(e)?}", "month = 6"} |
        Foreach-Object {$_ -replace "month\s*=\s*{(j|J)ul(y)?}", "month = 7"} |
        Foreach-Object {$_ -replace "month\s*=\s*{(a|A)ug(ust)?}", "month = 8"} |
        Foreach-Object {$_ -replace "month\s*=\s*{(s|S)ep(tember)?}", "month = 9"} |
        Foreach-Object {$_ -replace "month\s*=\s*{(o|O)ct(ober)?}", "month = 10"} |
        Foreach-Object {$_ -replace "month\s*=\s*{(n|N)ov(ember)?}", "month = 11"} |
        Foreach-Object {$_ -replace "month\s*=\s*{(d|D)ec(ember)?}", "month = 12"} |
        Set-Content "$MainDir\bibtex\PHD_unsorted.bib"
biber --tool --configfile="$MainDir/tools/biber-sort.conf" --output_indent=4 --output_fieldcase=title --output-align --output-file="$MainDir/bibtex/PHD.bib" --validate-config "$MainDir/bibtex/PHD_unsorted.bib"
Remove-Item "$MainDir\bibtex\PHD_unsorted.bib"

#Write-Host "---- Validate ----"
#biber --tool --validate-datamodel "$MainDir/bibtex/PHD.bib"


Write-Host "---- Done! ----"
