# TUM PhD Thesis Template

License: Public License (Do what you want)

[Example PDF (Generated via CI Pipeline)](https://gitlab.com/Pro1/phd-thesis-template/-/jobs/artifacts/main/raw/Thesis.pdf?job=job_build)

## Full example

I used this template for my PhD Thesis:

**From Unpacking to Plug & Produce –
Flexible Components Integration for Robots in Industry 4.0**

https://mediatum.ub.tum.de/doc/1552194/sebolv6v17hbw1ubv29w09hvr.2021-07-04%20final%20submission.pdf

## Setup

Adapt the files as necessary.

Don't forget to update the PDF Metadata in `settings/layout.tex` file.

## Build

To build the Thesis, simply run make:

```bash
make
```
